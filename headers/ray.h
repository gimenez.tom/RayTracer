#ifndef RAY_H
#define RAY_H

#include "vector3.h"

class Ray
{
public:
    Ray();
    Ray(const Vector3& origin, const Vector3& direction);

    Vector3 getOrigin() const;
    Vector3 getDirection() const;

private:
    Vector3 m_origin;
    Vector3 m_direction;
};

#endif //RAY_H
