#ifndef SPHERE_H
#define SPHERE_H

#include "hitable.h"

class Sphere : public Hitable
{
public:
    Sphere();
    Sphere(const Vector3& center, float radius);

    bool hit(const Ray& ray, float tMin, float tMax, HitRecord& record) const;

private:
    Vector3 m_center;
    float m_radius;
};

#endif //SPHERE_H
