#ifndef VECTOR3_H
#define VECTOR3_H

class Vector3
{
public:
    static const float EPSILON;
    static const Vector3 ZERO;

    Vector3();
    Vector3(float x, float y, float z);

    float x() const;
    float y() const;
    float z() const;
    float r() const;
    float g() const;
    float b() const;

    Vector3 operator+(const Vector3& v) const;
    Vector3 operator-(const Vector3& v) const;
    Vector3 operator-() const;
    Vector3 operator*(float s) const;
    Vector3 operator/(float s) const;

    bool operator==(const Vector3& v) const;
    bool operator!=(const Vector3& v) const;

    Vector3& operator+=(const Vector3& v);
    Vector3& operator-=(const Vector3& v);
    Vector3& operator*=(float s);
    Vector3& operator/=(float s);

    float magnitude() const;
    void normalize();

    static float Dot(const Vector3& u, const Vector3& v);

private:
    float m_x;
    float m_y;
    float m_z;

};

#endif //VECTOR3_H
