#ifndef HITABLE_H
#define HITABLE_H

#include "ray.h"

class Hitable
{
public:
    struct HitRecord
    {
        float t;
        Vector3 position;
        Vector3 normal;
    };

    virtual ~Hitable() {}

    virtual bool hit(const Ray& ray, float tMin, float tMax, HitRecord& record) const = 0;
};

#endif //HITABLE_H
