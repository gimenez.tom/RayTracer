#ifndef HITABLELIST_H
#define HITABLELIST_H

#include "hitable.h"

#include <vector>

class HitableList : public Hitable
{
public:
    HitableList();
    ~HitableList();

    bool hit(const Ray& ray, float tMin, float tMax, HitRecord& record) const;

    size_t size() const;
    void add(Hitable* hitable);

private:
    std::vector<Hitable*> m_hitables;
};

#endif //HITABLELIST_H
