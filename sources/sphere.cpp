#include "../headers/sphere.h"

#include <cmath>

Sphere::Sphere()
    : m_center(Vector3::ZERO)
    , m_radius(0.0f)
{

}

Sphere::Sphere(const Vector3& center, float radius)
    : m_center(center)
    , m_radius(radius)
{

}

bool Sphere::hit(const Ray &ray, float tMin, float tMax, Hitable::HitRecord &record) const
{
    Vector3 sphereToOrigin = ray.getOrigin() - m_center;
    float a = Vector3::Dot(ray.getDirection(), ray.getDirection());
    float b = 2.0f * Vector3::Dot(sphereToOrigin, ray.getDirection());
    float c = Vector3::Dot(sphereToOrigin, sphereToOrigin) - (m_radius * m_radius);
    float discriminant = (b * b) - (4.0f * a * c);

    if (discriminant > 0.0f)
    {
        float t = ((-b - sqrt(discriminant)) / (2.0f * a));
        if (t > tMin && t < tMax)
        {
            record.t = t;
            record.position = ray.getOrigin() + ray.getDirection() * t;
            record.normal = (record.position - m_center) / m_radius;
            return true;
        }

        t = ((-b + sqrt(discriminant)) / (2.0f * a));
        if (t > tMin && t < tMax)
        {
            record.t = t;
            record.position = ray.getOrigin() + ray.getDirection() * t;
            record.normal = (record.position - m_center) / m_radius;
            return true;
        }
    }
    return false;
}
