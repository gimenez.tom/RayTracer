#include "../headers/vector3.h"

#include <cmath>

const float Vector3::EPSILON = 0.0001f;
const Vector3 Vector3::ZERO = Vector3(0, 0, 0);

Vector3::Vector3()
    : m_x(0.0f)
    , m_y(0.0f)
    , m_z(0.0f)
{
}

Vector3::Vector3(float x, float y, float z)
    : m_x(x)
    , m_y(y)
    , m_z(z)
{
}

float Vector3::x() const
{
    return m_x;
}

float Vector3::y() const
{
    return m_y;
}
float Vector3::z() const
{
    return m_z;
}

float Vector3::r() const
{
    return m_x;
}

float Vector3::g() const
{
    return m_y;
}

float Vector3::b() const
{
    return m_z;
}

Vector3 Vector3::operator+(const Vector3& v) const
{
    return Vector3(m_x + v.x(), m_y + v.y(), m_z + v.z());
}

Vector3 Vector3::operator-(const Vector3& v) const
{

    return Vector3(m_x - v.x(), m_y - v.y(), m_z - v.z());
}

Vector3 Vector3::operator-() const
{
    return Vector3(-m_x, -m_y, -m_z);
}

Vector3 Vector3::operator*(float s) const
{
    return Vector3(m_x * s, m_y * s, m_z * s);
}

Vector3 Vector3::operator/(float s) const
{
    return Vector3(m_x / s, m_y / s, m_z / s);
}

bool Vector3::operator==(const Vector3& v) const
{
    return m_x == v.x() && m_y == v.y() && m_z == v.z();
}

bool Vector3::operator!=(const Vector3& v) const
{
    return m_x != v.x() && m_y != v.y() && m_z != v.z();
}

Vector3& Vector3::operator+=(const Vector3& v)
{
    m_x += v.x();
    m_y += v.y();
    m_z += v.z();

    return *this;
}

Vector3& Vector3::operator-=(const Vector3& v)
{
    m_x -= v.x();
    m_y -= v.y();
    m_z -= v.z();

    return *this;
}

Vector3& Vector3::operator*=(float s)
{
    m_x *= s;
    m_y *= s;
    m_z *= s;

    return *this;
}

Vector3& Vector3::operator/=(float s)
{
    m_x /= s;
    m_y /= s;
    m_z /= s;

    return *this;
}

float Vector3::magnitude() const
{
    return sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
}

void Vector3::normalize()
{
    float m = magnitude();
    if (m <= EPSILON)
    {
        return;
    }

    m_x /= m;
    m_y /= m;
    m_z /= m;

    if (fabs(m_x) <= EPSILON) m_x = 0.0f;
    if (fabs(m_y) <= EPSILON) m_y = 0.0f;
    if (fabs(m_z) <= EPSILON) m_z = 0.0f;
}

float Vector3::Dot(const Vector3& u, const Vector3& v)
{
    return u.x() * v.x() + u.y() * v.y() + u.z() * v.z();
}
