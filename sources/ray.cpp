#include "../headers/ray.h"

Ray::Ray()
    : m_origin(Vector3::ZERO)
    , m_direction(Vector3::ZERO)
{

}

Ray::Ray(const Vector3& origin, const Vector3& direction)
    : m_origin(origin)
    , m_direction(direction)
{

}

Vector3 Ray::getOrigin() const
{
    return m_origin;
}

Vector3 Ray::getDirection() const
{
    return m_direction;
}
