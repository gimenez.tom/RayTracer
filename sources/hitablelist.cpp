#include "../headers/hitablelist.h"

HitableList::HitableList()
{

}

HitableList::~HitableList()
{

}

bool HitableList::hit(const Ray &ray, float tMin, float tMax, Hitable::HitRecord &record) const
{
    bool hitAnything = false;
    float closestDistanceSoFar = tMax;
    Hitable::HitRecord tmpRecord;
    for (Hitable* hitable : m_hitables)
    {
        if (hitable->hit(ray, tMin, closestDistanceSoFar, tmpRecord) == true)
        {
            hitAnything = true;
            closestDistanceSoFar = record.t;
        }
    }

    if (hitAnything == false)
    {
        return false;
    }

    record = tmpRecord;

    return true;
}

size_t HitableList::size() const
{
    return m_hitables.size();
}

void HitableList::add(Hitable *hitable)
{
    m_hitables.push_back(hitable);
}
