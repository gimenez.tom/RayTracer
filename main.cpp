#include <iostream>
#include <cmath>

#include "headers/hitablelist.h"
#include "headers/sphere.h"

float HitSphere(const Vector3& center, float radius, const Ray& ray)
{
    Vector3 sphereToOrigin = ray.getOrigin() - center;
    float a = Vector3::Dot(ray.getDirection(), ray.getDirection());
    float b = 2.0f * Vector3::Dot(sphereToOrigin, ray.getDirection());
    float c = Vector3::Dot(sphereToOrigin, sphereToOrigin) - (radius * radius);
    float discriminant = (b * b) - (4.0f * a * c);

    if (discriminant < 0.0f)
    {
        return -1.0f;
    }
    else
    {
        return ((-b - sqrt(discriminant)) / (2.0f * a));
    }
    return discriminant > 0;
}

Vector3 ComputeColor(const Ray& ray, const Hitable& hitable)
{
    Hitable::HitRecord record;
    //float t = HitSphere(Vector3(0, 0, -1.0f), 0.5f, ray);
    if (hitable.hit(ray, 0, 100000, record))
    {
        Vector3 collisionPoint = ray.getOrigin() + ray.getDirection() * record.t;
        Vector3 normal = collisionPoint - Vector3(0, 0, -1.0f);
        normal.normalize();

        return Vector3(normal.x() + 1.0f, normal.y() + 1.0f, normal.z() + 1.0f) * 0.5f;
    }

    Vector3 direction = ray.getDirection();
    direction.normalize();

    float t = 0.5f * (direction.y() + 1.0f);
    return Vector3(1.0f, 1.0f, 1.0f) * (1.0f - t) + Vector3(0.5f, 0.7f, 1.0f) * t;
}

int main(int argc, char *argv[])
{
    uint16_t width = 200;
    uint16_t height = 100;

    Vector3 lowerLeftCorner(-2.0f, -1.0f, -1.0f);
    Vector3 horizontal(4.0f, 0.0f, 0.0f);
    Vector3 vertical(0.0f, 2.0f, 0.0f);
    Vector3 origin = Vector3::ZERO;

    Sphere sphere00(Vector3(0, 0, -1.0f), 0.5f);
    Sphere sphere01(Vector3(0, -100.5f, -1.0f), 100.0f);

    HitableList root;
    root.add(&sphere00);
    root.add(&sphere01);

    std::cout << "P3\n" << width << " " << height << "\n255\n";
    for (int16_t j = height-1; j >= 0; --j)
    {
        for (int16_t i = 0; i < width; ++i)
        {
            float u = static_cast<float>(i) / static_cast<float>(width);
            float v = static_cast<float>(j) / static_cast<float>(height);

            Ray ray(origin, lowerLeftCorner + (horizontal * u) + (vertical * v));

            Vector3 color = ComputeColor(ray, root);
            uint32_t red = static_cast<uint32_t>(255.99f * color.r());
            uint32_t green = static_cast<uint32_t>(255.99f * color.g());
            uint32_t blue = static_cast<uint32_t>(255.99f * color.b());

            std::cout << red << " " << green << " " << blue << "\n";
        }
    }

    return 0;
}
